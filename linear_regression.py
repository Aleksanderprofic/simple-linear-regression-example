import matplotlib.pyplot as plt

theta_0 = 0
theta_1 = 0

# Let's say X set is how old were cars and y set is how fast they were on a speed control
X = [5, 7, 8, 7, 2, 17, 2, 9, 4, 11, 12, 9, 6]
y = [99, 86, 87, 88, 105, 75, 103, 87, 94, 78, 77, 85, 86]

m = len(y)


def gradient_descent(th_0, th_1, length, alpha=0.002):
    sum_0 = 0
    sum_1 = 0
    for i in range(length):
        difference = h_x(th_0, th_1, X[i]) - y[i]
        sum_0 += difference
        sum_1 += difference * X[i]

    th_0 -= alpha * 1 / length * sum_0
    th_1 -= alpha * 1 / length * sum_1

    return th_0, th_1


def h_x(th_0, th_1, x_i):
    return th_0 + th_1 * x_i


def error_f(th_0, th_1, length):
    s = 0
    for i in range(length):
        s += (h_x(th_0, th_1, X[i]) - y[i]) ** 2

    return 1 / (2 * length) * s


before_error = 0
actual_error = error_f(theta_0, theta_1, m)

while abs(actual_error - before_error) > 0.0000000000001:
    theta_0, theta_1 = gradient_descent(theta_0, theta_1, m)

    before_error = actual_error
    actual_error = error_f(theta_0, theta_1, m)


predicted_y = list((h_x(theta_0, theta_1, x_i) for x_i in X))

plt.scatter(X, y)
plt.plot(X, predicted_y)

plt.show()
